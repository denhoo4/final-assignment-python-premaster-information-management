# Calculator by Dennis Hoogeveen, 2021
# Documented in git, https://gitlab.com/denhoo4/final-assignment-python-premaster-information-management

## Define functions and starting number
add = lambda x, y: x+y
subtract = lambda x, y: x-y
multiply = lambda x, y: x*y
divide = lambda x, y: x/y
current_number = 0

# Function to format floats to pretty print
def pretty_print(printable):
    if(printable == 0):
        return "0" 
    else:
        return str(printable).rstrip("0").rstrip(".") 

# -----------------------------------
# Run code
while(True):
    enter = input('Enter: ')

    # When q or Q is entered, print end value and break
    if(enter == 'q' or enter == 'Q'): 
        print('End value: ' + pretty_print(current_number))
        break

    # Seperate the operator and inputed value
    operator = enter[0:1] 

    # Try to get the inputed value
    try: 
        value = float(enter[1:]) 

    # If this is not a correct value, throw error and start loop anew
    except: 
        print('This is not a correct value.')
        print("Current value: " + pretty_print(current_number))
        print('---------------------------------')
        continue

    if operator == '+':
        current_number = add(current_number, value)
    elif operator == '-':
        current_number = subtract(current_number, value)
    elif operator == '*':
        current_number = multiply(current_number, value)
    elif operator == '/':
        current_number = divide(current_number, value)

    # If the first caracter from the input is not a operator, throw error
    else: 
        print('This is not a correct operator.')

    # Print the current value
    print("Current value: " + pretty_print(current_number))
    # Seperating line for readability
    print('---------------------------------') 
