# Final assignment Python - Premaster Information Management

In dit project vind je een werkende rekenmachine, geschreven in Python door mij. Deze rekenmachine was een opdracht die ik moest doen binnen mijn premaster Information Management.

Door de calculator op te starten in een terminal kan hij worden gebruikt, het startpunt is altijd 0. Nadat dit is gedaan dient men eerst een operator (+, -, *, /) te typen en daarna een getal. Bijvoorbeeld:

+5

Dit zal resulteren in 0 + 5 = 5